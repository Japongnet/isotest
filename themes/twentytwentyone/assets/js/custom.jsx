const controlButtons = document.querySelectorAll('.control-button');

controlButtons.forEach(function(btn) {
  btn.addEventListener('click', function() {
    var classAdd = this.innerHTML;
    var element = document.getElementById("stage");
    element.className = "";
    element.classList.add(classAdd);
  });
});