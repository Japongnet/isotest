<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since 1.0.0
 */

get_header();

?>
<div class="content-page">
	<input type="radio" class="view-select" id="normal" name="view" value="normal">
	<label class="view-select-label" for="normal">normal</label>
	<input type="radio" class="view-select"  id="iso-top" name="view" value="iso-top">
	<label class="view-select-label" for="iso-top">iso-top</label><br>
	<input type="radio" class="view-select"  id="iso-left" name="view" value="iso-left">
	<label class="view-select-label" for="female">iso-left</label><br>
	<input type="radio" class="view-select"  id="iso-right" name="view" value="iso-right">
	<label class="view-select-label" for="iso-right">iso-right</label>
	<input type="radio" class="view-select"  id="left" name="view" value="left">
	<label class="view-select-label" for="left">left</label>
	<input type="radio" class="view-select"  id="right" name="view" value="right">
	<label class="view-select-label" for="right">right</label>
	<input type="radio" class="view-select"  id="top" name="view" value="top">
	<label class="view-select-label" for="top">top</label>
	<input type="radio" class="view-select"  id="spin" name="view" value="spin">
	<label class="view-select-label" for="spin">spin</label>
	<input type="checkbox" class="hue-select"  id="hue" name="hue" value="hue">
	<label class="view-select-label" for="hue">hue</label>

	<div id="stage">
		<?php
		$co_ords = array();
		for ( $y = 0; $y <= 9; $y++ ) {
			for ( $x = 0; $x <= 9; $x++ ) {
				$base_x  = $x * 10;
				$rand_x1 = $base_x;
				$rand_x2 = $base_x + 10;
				$rand_y1 = $base_y;
				$rand_y2 = $base_y + 10;
				$rand_z1 = mt_rand( 1, 99 );
				$rand_z2 = mt_rand( 1, 99 );
				$co_ords[] = array( 'x1' => $rand_x1, 'x2' => $rand_x2, 'y1' => $rand_y1, 'y2' => $rand_y2, 'z1' => $rand_z1, 'z2' => $rand_z2 );
			}
			$base_y = $y * 10;
		}

		foreach ( $co_ords as $co_ords_set ) {
			echo '<div class="prism" style="';
			if ( $co_ords_set['x2'] < $co_ords_set['x1'] ) {
				echo ' --x1: ' . ( $cow_ords_set['x2'] ) . '; ';
				echo ' --x2: ' . ( $co_ords_set['x1'] ) . '; ';
			} else {
				echo ' --x1: ' . ( $co_ords_set['x1'] ) . '; ';
				echo ' --x2: ' . ( $co_ords_set['x2'] ) . '; ';
			}
			if ( $co_ords_set['y2'] < $co_ords_set['y1'] ) {
				echo ' --y1: ' . ( $co_ords_set['y2'] ) . '; ';
				echo ' --y2: ' . ( $co_ords_set['y1'] ) . '; ';
			} else {
				echo ' --y1: ' . ( $co_ords_set['y1'] ) . '; ';
				echo ' --y2: ' . ( $co_ords_set['y2'] ) . '; ';
			}
			if ( $co_ords_set['z2'] < $co_ords_set['z1'] ) {
				echo ' --z1: ' . ( $co_ords_set['z2'] ) . '; ';
				echo ' --z2: ' . ( $co_ords_set['z1'] ) . '; ';
			} else {
				echo ' --z1: ' . ( $co_ords_set['z1'] ) . '; ';
				echo ' --z2: ' . ( $co_ords_set['z2'] ) . '; ';
			}
			echo '">';
			echo '<div class="prism-pane x">
			</div>
				<div class="prism-pane y">
			</div>
				<div class="prism-pane z">
			</div>';
			echo '</div>';
		}
		?>
	</div>

<!--
	<div class="controls">
		<button id="button-normal" class="control-button">normal</button>
		<button id="button-iso-top" class="control-button">iso-top</button>
		<button id="button-iso-left" class="control-button">iso-left</button>
		<button id="button-iso-right" class="control-button">iso-right</button>
		<button id="button-top" class="control-button">top</button>
		<button id="button-left" class="control-button">left</button>
		<button id="button-right" class="control-button">right</button>
	</div>

	<div class="numbers">
	</div>
	-->
</div>
<?php



get_footer();
